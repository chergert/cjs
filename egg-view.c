/* egg-view.c
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "egg-marshal.h"
#include "egg-view.h"

G_DEFINE_TYPE (EggView, egg_view, G_TYPE_OBJECT)

/**
 * SECTION:egg-view
 * @title: EggView
 * @short_description: 
 *
 * 
 */

struct _EggViewPrivate
{
    gpointer dummy;
};

enum
{
    PROP_0,
};

static GList *resolvers = NULL;

/*
 *-----------------------------------------------------------------------------
 *
 * Public Methods
 *
 *-----------------------------------------------------------------------------
 */

/**
 * egg_view_add_resolver:
 * @resolver: An #EggViewResolver
 * @user_data: user data for @resolver.
 *
 * Registers a callback to be used to resolve an #EggView from a given view
 * path.
 */
void
egg_view_add_resolver (EggViewResolver callback,
                       gpointer        user_data)
{
	GClosure *closure;

	g_return_if_fail (callback != NULL);

	closure = g_cclosure_new (G_CALLBACK (callback), user_data, NULL);
	egg_view_add_resolver_closure (closure);
}

/**
 * egg_view_add_resolver_closure:
 * @closure: A #GClosure
 *
 * Adds a view resolver using a #GClosure. See egg_view_add_resolver().
 */
void
egg_view_add_resolver_closure (GClosure *closure)
{
	g_return_if_fail (closure != NULL);
	resolvers = g_list_append (resolvers, g_closure_ref (closure));
}

/**
 * egg_view_resolve:
 * @path: a string containing the path
 *
 * Resolves an #EggView for a given path.
 *
 * Return value: an #EggView or %NULL.
 */
EggView*
egg_view_resolve (const gchar *path)
{
	EggView *view;
	GList   *list;
	GValue   return_value = {0,},
	         params       = {0,};

	g_return_val_if_fail (path != NULL, NULL);

	for (list = resolvers; list; list = list->next) {
		g_value_init (&return_value, EGG_TYPE_VIEW);
		g_value_init (&params, G_TYPE_STRING);
		g_value_set_string (&params, path);
		g_closure_invoke (list->data, &return_value, 1, &params, NULL);
		view = g_value_get_object (&return_value);
		g_value_unset (&return_value);
		g_value_unset (&params);
		if (view)
			break;
	}

	return view;
}

/**
 * egg_view_new:
 *
 * Creates a new instance of #EggView.
 *
 * Return value: the newly created #EggView instance.
 */
EggView*
egg_view_new (void)
{
	return g_object_new (EGG_TYPE_VIEW, NULL);
}

/*
 *-----------------------------------------------------------------------------
 *
 * Class Methods
 *
 *-----------------------------------------------------------------------------
 */

static void
egg_view_finalize (GObject *object)
{
	EggViewPrivate *priv;

	g_return_if_fail (EGG_IS_VIEW (object));

	priv = EGG_VIEW (object)->priv;

	G_OBJECT_CLASS (egg_view_parent_class)->finalize (object);
}

static void
egg_view_class_init (EggViewClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = egg_view_finalize;
	g_type_class_add_private (object_class, sizeof (EggViewPrivate));
}

static void
egg_view_init (EggView *view)
{
	view->priv = G_TYPE_INSTANCE_GET_PRIVATE (view,
	                                          EGG_TYPE_VIEW,
	                                          EggViewPrivate);
}
