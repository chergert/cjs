/* egg-controller.h
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __EGG_CONTROLLER_H__
#define __EGG_CONTROLLER_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define EGG_TYPE_CONTROLLER            (egg_controller_get_type())
#define EGG_CONTROLLER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), EGG_TYPE_CONTROLLER, EggController))
#define EGG_CONTROLLER_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), EGG_TYPE_CONTROLLER, EggController const))
#define EGG_CONTROLLER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  EGG_TYPE_CONTROLLER, EggControllerClass))
#define EGG_IS_CONTROLLER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EGG_TYPE_CONTROLLER))
#define EGG_IS_CONTROLLER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  EGG_TYPE_CONTROLLER))
#define EGG_CONTROLLER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  EGG_TYPE_CONTROLLER, EggControllerClass))

typedef struct _EggController        EggController;
typedef struct _EggControllerClass   EggControllerClass;
typedef struct _EggControllerPrivate EggControllerPrivate;

/**
 * EggControllerResolver:
 * @path: a #GStrv containing the lookup path
 * @user_data: user data provided to egg_controller_add_resolver().
 *
 * A resolver that can locate a controller for a given path.
 *
 * Return value: 
 */
typedef EggController* (*EggControllerResolver) (gchar    **path,
                                                 gpointer   user_data);

struct _EggController
{
	GObject parent;

	/*< private >*/
	EggControllerPrivate *priv;
};

struct _EggControllerClass
{
	GObjectClass parent_class;

	void (*show) (EggController *controller);
	void (*hide) (EggController *controller);
};

void           egg_controller_add_resolver   (EggControllerResolver resolver,
                                              gpointer              user_data);
EggController* egg_controller_resolve        (const gchar *path);
EggController* egg_controller_resolvev       (gchar **path);
EggController* egg_controller_resolve_printf (const gchar *format,
                                              ...) G_GNUC_PRINTF (1, 2);
GType          egg_controller_get_type       (void) G_GNUC_CONST;
void           egg_controller_show           (EggController *controller);
void           egg_controller_hide           (EggController *controller);

G_END_DECLS

#endif /* __EGG_CONTROLLER_H__ */
