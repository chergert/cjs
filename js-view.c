/* js-view.c
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <glib-object.h>
#include <gjs/gjs.h>
#include <jsapi.h>

#include "egg-view.h"

typedef struct
{
	GjsContext *context;
} JSData;

/**
 * js_view_register:
 *
 * Registers an EggViewResolver for attaching views from JavaScript.
 */
void
js_view_register (gchar **search_path)
{
	static JSData *data;
	GDir          *dir;
	gchar         *path;
	const gchar   *name;
	gint           i;

	data = g_slice_new0 (JSData);
	data->context = gjs_context_new_with_search_path (search_path);

	for (i = 0; search_path [i]; i++) {
		if (!(dir = g_dir_open (search_path [i], 0, NULL)))
			continue;

		/*
		 * Load the special file init.js first if it exists.
		 */

		path = g_build_filename (search_path [i], "init.js", NULL);
		if (g_file_test (path, G_FILE_TEST_IS_REGULAR)) {
			gjs_context_eval_file (data->context, path, NULL, NULL);
		}
		g_free (path);

		/*
		 * Load the rest of the files in directory.
		 */

		while (NULL != (name = g_dir_read_name (dir))) {
			path = g_build_filename (search_path [i], name, NULL);
			if (g_str_has_suffix (path, ".js") && g_strcmp0 (name, "init.js") != 0)
				gjs_context_eval_file (data->context, path, NULL, NULL);
			g_free (path);
		}
	}
}
