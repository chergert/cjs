/* SourcesToolbox.js
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const GtkClutter = imports.gi.GtkClutter;
const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const GObject = imports.gi.GObject;

let SourcesToolbox = null;

Views.register("/Mock", function(path, controller) {
   if (!SourcesToolbox) {
      SourcesToolbox = new GtkClutter.Window();
      SourcesToolbox.title = "Data Sources";
      SourcesToolbox.set_type_hint(Gdk.WindowTypeHint.UTILITY);
      SourcesToolbox.set_default_size(200, 350);

      let vbox = new Gtk.VBox();
      SourcesToolbox.add(vbox);
      vbox.show();

      let scroller = new Gtk.ScrolledWindow();
      scroller.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);
      vbox.pack_start(scroller, true, true, 0);
      scroller.show();

      var treeview = new Gtk.TreeView();
      treeview.set_headers_visible(false);
      scroller.add(treeview);
      treeview.show();

      let column = new Gtk.TreeViewColumn();
      treeview.append_column(column);

      let cell = new Gtk.CellRendererText();
      column.pack_start(cell, true);
      column.add_attribute(cell, "text", 0);

      SourcesToolbox.connect("delete-event", function() {
         SourcesToolbox.hide();
         return true;
      });

      let hbox = new Gtk.HBox();
      vbox.pack_start(hbox, false, true, 0);
      hbox.show();

      let refresh = new Gtk.Button();
      let image = new Gtk.Image();
      refresh.set_relief(Gtk.ReliefStyle.NONE);
      image.set_from_stock(Gtk.STOCK_REFRESH, Gtk.IconSize.MENU);
      refresh.add(image);
      image.show();
      refresh.show();
      hbox.pack_start(refresh, false, true, 0);

      SourcesToolbox.show();

      var stage = SourcesToolbox.get_stage();
      refresh.connect('clicked', function() {
         var r = new Clutter.Rectangle();
         let c = new Clutter.Color();
         c.from_string('#000000aa');
         r.set_color(c);
         r.set_opacity(0);
         new Animation(r).fadeIn();
         stage.add_actor(r);
         r.set_position(0,0);
         r.set_size(200, 322);
         r.show();
         var l = new Clutter.Text();
         l.set_text('Refreshing');
         c = new Clutter.Color();
         c.from_string('#ffffffff');
         l.set_color(c);
         stage.add_actor(l);
         l.set_position(65, 100);
         l.show();

         Mainloop.timeout_add(3000, function() {
            l.hide();
            let m = new Gtk.ListStore();
            m.set_column_types(1, [GObject.type_from_name('gchararray')]);
            let i = new Gtk.TreeIter();
            m.append(i);
            m.set_value(i, 0, 'Memory Profiler');
            treeview.set_model(m);
            let a = new Animation(r);
            a.fadeOut();
            return false;
         });
      });
   }

   return SourcesToolbox;
});
