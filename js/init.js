/* init.js
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const Clutter = imports.gi.Clutter;
const Egg = imports.gi.Egg;

var Views = {
   register: function(path, factory) {
      this._factories[path] = factory;
   },

   _factories: {}
}

Egg.View.add_resolver_closure(function(path) {
   if (Views._factories[path] != undefined) {
      /* This isn't doing anything yet, we need to wire stuff up */
      let view = new Egg.View();
      Views._factories[path]();
   }
});

function Animation(actor) {
   this._init(actor);
}

Animation.prototype = {
   _init: function(actor) {
      this._actor = actor;
   },

   fade: function(level, mode, duration) {
      if (level < 0 || level > 255)
         throw new Error("level must be >= 0 || level <= 255");

      if (!duration)
         duration = this._duration;

      if (!mode)
         mode = this._mode;

      Egg.anim_fade(this._actor, mode, duration, level);

      return this;
   },

   fadeOut: function(mode, duration) {
      return this.fade(0, mode, duration);
   },

   fadeIn: function(mode, duration) {
      return this.fade(255, mode, duration);
   },

   _duration: 1000,
   _mode    : Clutter.AnimationMode.LINEAR
};
