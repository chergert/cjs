/* mock-controller.h
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __MOCK_CONTROLLER_H__
#define __MOCK_CONTROLLER_H__

#include <glib-object.h>

#include "egg-controller.h"

G_BEGIN_DECLS

#define MOCK_TYPE_CONTROLLER            (mock_controller_get_type())
#define MOCK_CONTROLLER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MOCK_TYPE_CONTROLLER, MockController))
#define MOCK_CONTROLLER_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), MOCK_TYPE_CONTROLLER, MockController const))
#define MOCK_CONTROLLER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  MOCK_TYPE_CONTROLLER, MockControllerClass))
#define MOCK_IS_CONTROLLER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MOCK_TYPE_CONTROLLER))
#define MOCK_IS_CONTROLLER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  MOCK_TYPE_CONTROLLER))
#define MOCK_CONTROLLER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  MOCK_TYPE_CONTROLLER, MockControllerClass))

typedef struct _MockController        MockController;
typedef struct _MockControllerClass   MockControllerClass;
typedef struct _MockControllerPrivate MockControllerPrivate;

struct _MockController
{
	EggController parent;

	/*< private >*/
	MockControllerPrivate *priv;
};

struct _MockControllerClass
{
	EggControllerClass parent_class;
};

GType          mock_controller_get_type (void) G_GNUC_CONST;
EggController* mock_controller_new      (void);

G_END_DECLS

#endif /* __MOCK_CONTROLLER_H__ */
