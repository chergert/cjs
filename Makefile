all: egg-test Egg-1.0.gir Egg-1.0.typelib

G_IR_SCANNER = g-ir-scanner
G_IR_COMPILER = g-ir-compiler

WARNINGS =								\
	-Wall -Werror -Wold-style-definition				\
	-Wdeclaration-after-statement					\
	-Wredundant-decls -Wmissing-noreturn -Wshadow -Wcast-align	\
	-Wwrite-strings -Winline -Wformat-nonliteral -Wformat-security	\
	-Wswitch-enum -Wswitch-default -Winit-self			\
	-Wmissing-include-dirs -Wundef -Waggregate-return		\
	-Wmissing-format-attribute -Wnested-externs			\
	-DJS_HAS_FILE_OBJECT=0 \
	$(NULL)

sources_c =								\
	egg-anim.c							\
	egg-controller.c						\
	egg-marshal.c							\
	egg-view.c							\
	mock-controller.c						\
	js-view.c							\
	main.c								\
	$(NULL)

sources_public_h =							\
	egg-anim.h							\
	egg-controller.h						\
	egg-view.h							\
	$(NULL)

sources_private_h =							\
	egg-marshal.h							\
	mock-controller.h						\
	js-view.h							\
	$(NULL)

CLEANFILES = egg-test Egg-1.0.gir Egg-1.0.typelib

PKGS = gtk+-2.0 gjs-1.0 clutter-gtk-0.90 clutter-1.0

egg-marshal.c egg-marshal.h: egg-marshal.list
	glib-genmarshal --prefix=egg_marshal --header egg-marshal.list > egg-marshal.h
	glib-genmarshal --prefix=egg_marshal --body egg-marshal.list > egg-marshal.c

Egg-1.0.gir Egg-1.0.typelib: $(sources_c) $(sources_public_h) $(sources_private_h) egg-test
	LD_LIBRARY_PATH=/usr/lib/xulrunner-devel-1.9.1.5/lib \
	$(G_IR_SCANNER) -v \
		--nsversion=1.0 \
		--add-include-path=. \
		--namespace Egg \
		--output Egg-1.0.gir \
		--program=./egg-test \
		--pkg gobject-2.0 \
		--pkg clutter-1.0 \
		--include=GObject-2.0 \
		--include=Clutter-1.0 \
		$(sources_c) \
		$(sources_public_h) \
		$(NULL)
	$(G_IR_COMPILER) Egg-1.0.gir -o Egg-1.0.typelib

egg-test: $(sources_c) $(sources_public_h) $(sources_private_h)
	$(CC) -g -o $@ $(WARNINGS) $(sources_c) `pkg-config --cflags --libs $(PKGS)`

clean:
	rm -rf $(CLEANFILES)

test:
	LD_LIBRARY_PATH=/usr/lib/xulrunner-devel-1.9.1.5/lib ./egg-test
