/* egg-view.h
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __EGG_VIEW_H__
#define __EGG_VIEW_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define EGG_TYPE_VIEW            (egg_view_get_type())
#define EGG_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), EGG_TYPE_VIEW, EggView))
#define EGG_VIEW_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), EGG_TYPE_VIEW, EggView const))
#define EGG_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  EGG_TYPE_VIEW, EggViewClass))
#define EGG_IS_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EGG_TYPE_VIEW))
#define EGG_IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  EGG_TYPE_VIEW))
#define EGG_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  EGG_TYPE_VIEW, EggViewClass))

typedef struct _EggView        EggView;
typedef struct _EggViewClass   EggViewClass;
typedef struct _EggViewPrivate EggViewPrivate;

/**
 * EggViewResolver:
 * @path: A #GStrv containing the path
 * @user_data: user data from egg_view_add_resolver().
 *
 * A callback prototype for resolving an #EggView for a given view path.
 */
typedef EggView* (*EggViewResolver) (gchar    *path,
                                     gpointer  user_data);

struct _EggView
{
	GObject parent;

	/*< private >*/
	EggViewPrivate *priv;
};

struct _EggViewClass
{
	GObjectClass parent_class;
};

GType    egg_view_get_type             (void) G_GNUC_CONST;
void     egg_view_add_resolver         (EggViewResolver   resolver,
                                        gpointer          user_data);
void     egg_view_add_resolver_closure (GClosure         *closure);
EggView* egg_view_resolve              (const gchar      *path);
EggView* egg_view_resolve_printf       (const gchar      *format,
                                        ...) G_GNUC_PRINTF (1, 2);
EggView* egg_view_new                  (void);

G_END_DECLS

#endif /* __EGG_VIEW_H__ */
