/* mock-controller.c
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>

#include "egg-view.h"
#include "mock-controller.h"

G_DEFINE_TYPE (MockController, mock_controller, EGG_TYPE_CONTROLLER)

/**
 * SECTION:mock-controller
 * @title: MockController
 * @short_description: 
 *
 * 
 */

struct _MockControllerPrivate
{
    EggView *view;
};

/*
 *-----------------------------------------------------------------------------
 *
 * Public Methods
 *
 *-----------------------------------------------------------------------------
 */

/**
 * mock_controller_new:
 *
 * Creates a new instance of #MockController.
 *
 * Return value: the newly created #MockController instance.
 */
EggController*
mock_controller_new (void)
{
	return g_object_new (MOCK_TYPE_CONTROLLER, NULL);
}

/*
 *-----------------------------------------------------------------------------
 *
 * Class Methods
 *
 *-----------------------------------------------------------------------------
 */

static void
mock_controller_finalize (GObject *object)
{
	MockControllerPrivate *priv;

	g_return_if_fail (MOCK_IS_CONTROLLER (object));

	priv = MOCK_CONTROLLER (object)->priv;

	G_OBJECT_CLASS (mock_controller_parent_class)->finalize (object);
}

static void
mock_controller_class_init (MockControllerClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = mock_controller_finalize;
	g_type_class_add_private (object_class, sizeof (MockControllerPrivate));
}

static void
mock_controller_init (MockController *controller)
{
	controller->priv = G_TYPE_INSTANCE_GET_PRIVATE (controller,
	                                                MOCK_TYPE_CONTROLLER,
	                                                MockControllerPrivate);
	controller->priv->view = egg_view_resolve ("/Mock");
}
