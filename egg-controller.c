/* egg-controller.c
 *
 * Copyright (c) 2009 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>

#include "egg-controller.h"
#include "egg-marshal.h"

G_DEFINE_ABSTRACT_TYPE (EggController, egg_controller, G_TYPE_OBJECT)

/**
 * SECTION:egg-controller
 * @title: EggController
 * @short_description: 
 *
 * 
 */

struct _EggControllerPrivate
{
    gpointer dummy;
};

typedef struct
{
	EggControllerResolver callback;
	gpointer              data;
} Resolver;

enum
{
	SHOW,
	HIDE,
    LAST_SIGNAL
};

static GList* resolvers = NULL;
static guint  signals [LAST_SIGNAL];

enum
{
    PROP_0,
};

/*
 *-----------------------------------------------------------------------------
 *
 * Public Methods
 *
 *-----------------------------------------------------------------------------
 */

/**
 * egg_controller_add_resolver:
 * @callback: An #EggControllerResolver
 * @user_data: user data for @resolver.
 *
 * Registers a callback to be used to resolve an #EggController from a given
 * controller path.
 */
void
egg_controller_add_resolver (EggControllerResolver callback,
                             gpointer              user_data)
{
	Resolver *resolver;

	g_return_if_fail (callback != NULL);

	resolver = g_slice_new0 (Resolver);
	resolver->callback = callback;
	resolver->data = user_data;
	resolvers = g_list_append (resolvers, resolver);
}

/**
 * egg_controller_resolvev:
 * @path: A #GStrv containing the path portions
 *
 * Resolves a controller for the path provided.
 *
 * Return value: An #EggController
 */
EggController*
egg_controller_resolvev (gchar **path)
{
	EggController *controller = NULL;
	GList         *list;
	Resolver      *resolver;

	g_return_val_if_fail (path != NULL, NULL);

	for (list = resolvers; list; list = list->next) {
		if (NULL != (resolver = list->data)) {
			controller = resolver->callback (path, resolver->data);
			if (controller)
				break;
		}
	}

	return controller;
}

/**
 * egg_controller_resolve:
 * @path: A controller path to lookup.
 *
 * Resolves a controller for a path.
 *
 * Return value: A resolved #EggController
 *
 * Side effects: The registered resolvers are executed.
 */
EggController*
egg_controller_resolve (const gchar *path)
{
	gchar         **vpath;
	EggController  *controller;

	g_return_val_if_fail (path != NULL, NULL);

	vpath = g_strsplit (path, "/", 0);
	controller = egg_controller_resolvev (vpath);
	g_strfreev (vpath);

	return controller;
}

/**
 * egg_controller_resolve_printf:
 * @format: A printf format for building a path
 *
 * Resolves a controller by first building a path and then calling
 * egg_controller_resolve().
 *
 * Return value: An #EggController or %NULL
 */
EggController*
egg_controller_resolve_printf (const gchar *format,
                               ...)
{
	EggController *controller = NULL;
	gchar         *path       = NULL;
	va_list        args;

	va_start (args, format);
	g_vasprintf (&path, format, args);
	va_end (args);

	if (path) {
		controller = egg_controller_resolve (path);
		g_free (path);
	}

	return controller;
}

/**
 * egg_controller_show:
 * @controller: An #EggController
 *
 * Shows the view attached to the controller.
 */
void
egg_controller_show (EggController *controller)
{
	gboolean shown = FALSE;
	g_return_if_fail (EGG_IS_CONTROLLER (controller));
	g_signal_emit (controller, signals [SHOW], 0, &shown);
}

/**
 * egg_controller_hide:
 * @controller: An #EggController
 *
 * Hides the view attached to the controller.
 */
void
egg_controller_hide (EggController *controller)
{
	gboolean hidden = FALSE;
	g_return_if_fail (EGG_IS_CONTROLLER (controller));
	g_signal_emit (controller, signals [HIDE], 0, &hidden);
}

/*
 *-----------------------------------------------------------------------------
 *
 * Class Methods
 *
 *-----------------------------------------------------------------------------
 */

static void
egg_controller_finalize (GObject *object)
{
	EggControllerPrivate *priv;

	g_return_if_fail (EGG_IS_CONTROLLER (object));

	priv = EGG_CONTROLLER (object)->priv;

	G_OBJECT_CLASS (egg_controller_parent_class)->finalize (object);
}

static void
egg_controller_class_init (EggControllerClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = egg_controller_finalize;
	g_type_class_add_private (object_class, sizeof (EggControllerPrivate));

	/**
	 * EggController::show:
	 * @controller: An #EggController
	 *
	 * The "show" signal.
	 */
	signals [SHOW] = g_signal_new ("show",
	                               EGG_TYPE_CONTROLLER,
								   G_SIGNAL_RUN_LAST,
								   G_STRUCT_OFFSET (EggControllerClass, show),
								   g_signal_accumulator_true_handled,
								   NULL,
	                               egg_marshal_BOOL__VOID,
								   G_TYPE_BOOLEAN,
								   0);

	/**
	 * EggController::hide:
	 * @controller: An #EggController
	 *
	 * The "hide" signal.
	 */
	signals [HIDE] = g_signal_new ("hide",
	                               EGG_TYPE_CONTROLLER,
								   G_SIGNAL_RUN_LAST,
								   G_STRUCT_OFFSET (EggControllerClass, hide),
								   g_signal_accumulator_true_handled,
								   NULL,
	                               egg_marshal_BOOL__VOID,
								   G_TYPE_BOOLEAN,
								   0);
}

static void
egg_controller_init (EggController *controller)
{
	controller->priv = G_TYPE_INSTANCE_GET_PRIVATE (controller,
	                                                EGG_TYPE_CONTROLLER,
	                                                EggControllerPrivate);
}
