#include <stdlib.h>
#include <gtk/gtk.h>
#include <girepository.h>
#include <clutter-gtk/clutter-gtk.h>

#include "egg-controller.h"
#include "egg-view.h"
#include "mock-controller.h"
#include "js-view.h"

static gchar *introspect = NULL;

static GOptionEntry entries[] = {
	{ "introspect-dump", '\0', 0, G_OPTION_ARG_STRING, &introspect, NULL, NULL },
	{ NULL }
};

static EggController*
controller_resolver (gchar    **parts,
                     gpointer   user_data)
{
	static EggController *controller = NULL;

	if (!controller)
		controller = mock_controller_new ();

	return controller;
}

gint
main (gint   argc,
      gchar *argv[])
{
	EggController *controller;
	gchar *path[2] = { NULL, NULL };
	GOptionContext *context;
	GError *error = NULL;

	context = g_option_context_new ("- controller test script");
	g_option_context_add_main_entries (context, entries, NULL);
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_printerr ("%s\n", error->message);
		return EXIT_FAILURE;
	}

	gtk_clutter_init (&argc, &argv);

	if (introspect) {
		if (!g_irepository_dump (introspect, &error)) {
			g_printerr ("%s\n", error->message);
			return EXIT_FAILURE;
		}
		return EXIT_SUCCESS;
	}

	g_irepository_prepend_search_path (g_path_get_dirname (argv [0]));
	path [0] = (gchar*)"js";
	js_view_register (path);
	egg_controller_add_resolver (controller_resolver, NULL);

	controller = egg_controller_resolve ("/Sources");
	egg_controller_show (controller);

	gtk_main ();

	return 0;
}
